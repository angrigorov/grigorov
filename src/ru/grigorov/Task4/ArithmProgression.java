package ru.grigorov.Task4;

import java.util.Scanner;

public class ArithmProgression {
    public static void main(String[] args) {
        System.out.println("Задайте начальное значение, шаг прогрессии и количество шагов");
        Scanner initialValueScanner = new Scanner(System.in);
        Scanner stepProgressionScanner = new Scanner(System.in);
        Scanner numberOfStepsScanner = new Scanner(System.in);
        int initialValue = initialValueScanner.nextInt();
        int stepProgression = stepProgressionScanner.nextInt();
        int numberOfSteps = numberOfStepsScanner.nextInt();
        System.out.print(initialValue + " ");
        for (int i = 0; i < numberOfSteps; i++) {
            System.out.print((initialValue + stepProgression) + " ");
            initialValue = initialValue + stepProgression;
        }
    }
}
