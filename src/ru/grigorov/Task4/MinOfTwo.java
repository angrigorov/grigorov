package ru.grigorov.Task4;

import java.util.Scanner;

public class MinOfTwo {
    public static void main(String[] args) {
        System.out.println("Сейчас мы определим минимальное из двух введенных вами чисел. Введите первое число, затем нажмите Enter и введите второе число:");
        Scanner numberOneScanner = new Scanner(System.in);
        Scanner numberTwoScanner = new Scanner(System.in);
        if (numberOneScanner.hasNextDouble() && numberTwoScanner.hasNextDouble()){
            double numberOne = numberOneScanner.nextDouble();
            double numberTwo = numberTwoScanner.nextDouble();
            if (numberOne < numberTwo){
                System.out.println("Минимальное число: " + numberOne);
            }
            else if (numberTwo < numberOne){
                System.out.println("Минимальное число: " + numberTwo);
            }
            else {
                System.out.println("Введенные вами числа равны");
            }
        }
        else {
            System.out.println("Что-то из введенного вами не является числом");
        }
        numberOneScanner.close();
        numberTwoScanner.close();
    }
}
