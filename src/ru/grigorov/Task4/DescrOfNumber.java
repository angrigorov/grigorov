package ru.grigorov.Task4;

import java.util.Scanner;

public class DescrOfNumber {
    public static void main(String[] args) {
        System.out.println("Введите целое число от -2147483648 до 2147483647, а программа расскажет вам некоторые его свойства:");
        Scanner numberScanner = new Scanner(System.in);
        if (numberScanner.hasNextInt()){
            System.out.print("Вы ввели ");
            double number = numberScanner.nextInt();
            if (number > 0){
                System.out.print("положительное ");
            }
            else if (number < 0){
                System.out.print("отрицательное ");
            }
                else {
                System.out.print("нулевое ");
            }
                if (number != 0 && number % 2 == 0){
                    System.out.print("четное ");
                }
                else if (number % 2 != 0){
                    System.out.print("нечетное ");
                }
        }
        else {
            System.out.print("Вы ввели не число, или не удовлетворяющее условиям ");
        }
        System.out.println("число");
        numberScanner.close();
    }
}
