package ru.grigorov.Task21_1;

/*Дан двумерный массив. Задача – написать метод public void toLeft() {}
1. Пройти с 1-ой до последней строки
2. Пройти с 1-го до предпоследнего элемента
3. В текущую ячейку поместить значение следующей
4. Последнему элементу присвоить 0*/

public class AppDZ1 {

    static int i;
    static int j;

    public static void main(String[] args) {
        int[][] massive =  {{5,7,3,8,9,3,2},{7,3,2,223,232,7,89,4,3}};
        System.out.println("Массив до преобразования:");
        printMassive(massive);
        toLeft(massive);
        System.out.println("Массив после преобразования:");
        printMassive(massive);
    }

    private static void printMassive(int[][] massive) {
        for (i = 0; i < massive.length; i++) {
            for (j = 0; j < massive[i].length; j++){
                System.out.print(massive[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void toLeft(int[][] massive) {
        for (i = 0; i < massive.length; i++) {
            for (j = 0;  j < massive[i].length; j++){
                if (j == massive[i].length - 1){
                    massive[i][j] = 0;
                } else massive[i][j] = massive[i][j+1];
            }
        }
    }
}
