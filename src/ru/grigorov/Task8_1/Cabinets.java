package ru.grigorov.Task8_1;

import java.util.Scanner;

public enum Cabinets {
    BASECABINET("Напольный шкаф", 5000),
    WALLCABINET("Настенный шкаф", 3000);
    private String typeOfCabinet;
    public int price;

    Cabinets(String typeOfCabinet, int price) {
        this.typeOfCabinet = typeOfCabinet;
        this.price = price;
    }

    public String getTypeOfCabinet() {
        return typeOfCabinet;
    }

    public int getPrice() {
        return price;
    }

    protected final static int baseCabinetCost(){ //этот метод нельзя переопределять, поэтому - final
        System.out.println("Для начала введите количество напольных шкафов:");
        Scanner baseCabinetNumberScanner = new Scanner(System.in);
        if (baseCabinetNumberScanner.hasNextInt()) {
            int baseCabinetNumber = baseCabinetNumberScanner.nextInt();
            return baseCabinetNumber*BASECABINET.price;
        }
        else {
            System.out.println("Введите целое число, цифрами");
            return baseCabinetCost();
        }
    }
    protected final static int wallCabinetCost() { //этот метод нельзя переопределять, поэтому - final
        System.out.println("Введите количество настенных шкафов:");
        Scanner wallCabinetNumberScanner = new Scanner(System.in);
        if (wallCabinetNumberScanner.hasNextInt()) {
            int wallCabinetNumber = wallCabinetNumberScanner.nextInt();
            return wallCabinetNumber*WALLCABINET.price;
        }
        else {
            System.out.println("Введите целое число, цифрами");
            return wallCabinetCost();
        }
    }
}
