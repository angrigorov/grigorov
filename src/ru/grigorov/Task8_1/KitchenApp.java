package ru.grigorov.Task8_1;
/*
1. Поэкспериментировать с ключевым словом final
*/

public final class KitchenApp { //В этой программе собираем мебель для кухни и считаем ее стоимость. Класс приложения нельзя
    // наследовать поэтому стоит модификатор final
    public static final int PRICEOFSINK = 8000; //В нашем мебельном магазине всегда только один вид раковин,
    // раковина обязательна к заказу. Поэтому цена раковины - константа, модификатор доступа - final.
    public static void main(String[] args) {

        Cabinets[] itemsCabinets = Cabinets.values();
        System.out.println("Выберите мебель для вашей новой кухни. Мы посчитаем ее стоимость.");
        System.out.println("1 Раковина 10000 рублей");
        for (int i = 2; i <= itemsCabinets.length+1; i++) {
            System.out.println(i+ " " +itemsCabinets[i-2].getTypeOfCabinet() + " " + itemsCabinets[i-2].getPrice() + " рублей");
        }
        System.out.println("Общая стоимость кухни составляет " + (Cabinets.baseCabinetCost() + Cabinets.wallCabinetCost()
                + PRICEOFSINK) + " рублей.");
    }
}
