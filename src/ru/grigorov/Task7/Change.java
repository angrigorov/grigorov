package ru.grigorov.Task7;

import static ru.grigorov.Task7.Money.getPutMoney;

public class Change {
    private int puttedMoney;
    private int change;

    public void getChange(int puttedMoney, int price){
        change = puttedMoney - price;
        if (change >= 0) {
            System.out.print("Ваша сдача составляет " + change + " руб. ");
            }
        else {
            System.out.println("Внесенных денег недостаточно, не хватает еще " + change*(-1) + " руб.");
            int addMoney = getPutMoney();
            getChange(puttedMoney + addMoney, price);
        }
    }
}
