package ru.grigorov.Task7;

public class NumberOfItemException extends Exception {
    @Override
    public String getMessage() {
        return "Товара с таким номером у нас нет.";
    }
}
