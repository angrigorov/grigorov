package ru.grigorov.Task7;

public enum GoodsList {
    COFFEEAMERICANO ("кофе \"Американо\"", 90),
    HOTCHOCOLATE("горячий шоколад", 100),
    COLA("кола", 60),
    STILLWATER("негазированная вода", 20);

    private String name;
    private int price;

    GoodsList(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
