package ru.grigorov.Task7;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Scanner;

/*Реализовать программу «Вендинговый автомат», которая позволит:
        Посмотреть меню напитков
        Внести деньги на внутренний счет
        Выбрать номер напитка и получить его, если на счету достаточно средств.
        Программа должна обрабатывать следующие ситуации:
        Пользователь не внес деньги
        Пользователь выбрал более дорогой напиток, чем внесено денег
        Пользователь выбрал несуществующий номер напитка
        Для хранения напитков предлагается использовать массив с enum (или с обычным классом). У напитка должна быть цена и название.*/
public class Vending {

    private static final Logger logger = LoggerFactory.getLogger(Vending.class);

    public static int change;

    private static GoodsList[] items = GoodsList.values();

    public static void main(String[] args) {
        logger.info("Начало работы программы");
        System.out.println("Вас приветствует вендинговый автомат \"Березка\"! Выберите интересующий вас напиток.");
        for (int i = 1; i <= items.length; i++) {
            System.out.println("Нажмите " + (i) + " для выбора напитка " + items[i-1].getName() + ". Цена "
                    + items[i-1].getPrice());

        }
    try {
        chooseYourDrink();
    }   catch (NumberOfItemException e){
        logger.error(e.getMessage(), e);
        System.out.println(e.getMessage());
    }
    }

    private static void chooseYourDrink() throws NumberOfItemException{
        Money bill = new Money();
        Change change = new Change();

        Scanner scanNumber = new Scanner(System.in);
        if (scanNumber.hasNextInt()) {
            int numberOfItem = scanNumber.nextInt();

            if (numberOfItem < 1 || numberOfItem > items.length) {
                throw new NumberOfItemException();
            } else {
                System.out.println("Вы выбрали " + items[numberOfItem - 1].getName());
                change.getChange(bill.getPutMoney(), items[numberOfItem - 1].getPrice());
                System.out.println("Ваш заказ - " + items[numberOfItem - 1].getName() + " - готов! Хорошего дня!");
            }
        }
        else {
            logger.warn("Пользователь ошибся в выборе номера товара");
            throw new NumberOfItemException();
        }
        scanNumber.close();
    }
}
