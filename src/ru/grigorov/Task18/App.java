package ru.grigorov.Task18;

//Написать программу, которая копирует файл с одной кодировкой в файл с другой.

import sun.nio.cs.KOI8_R;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws IOException {
        String fileName = "BaseFile.txt";
        try (Writer writer = new OutputStreamWriter(
                new FileOutputStream(fileName)
        )) {
            writer.write("Текст, предназначенный для перекодирования в другом стандарте");
        }

        System.out.println("Введите кодировку, в которую надо конвертировать базовый файл (KOI8-R, Windows-1251, UTF8):");
        Scanner scanner = new Scanner(System.in);
        String choiceOfEncoding = scanner.nextLine();

        switch (choiceOfEncoding){
            case "KOI8-R":
                try (InputStream is = new FileInputStream(fileName)) {
                byte[] buf = new byte[1000];
                if(is.read(buf) != -1) {
                    String s = new String(buf, "KOI8-R");
                    try (Writer writer = new OutputStreamWriter(
                            new FileOutputStream("new.txt")
                    )) {
                        writer.write(s);
                    }
                }
            }
                break;
            case "Windows-1251":
                try (InputStream is = new FileInputStream(fileName)) {
                    byte[] buf = new byte[1000];
                    if(is.read(buf) != -1) {
                        String s = new String(buf, "Windows-1251");
                        try (Writer writer = new OutputStreamWriter(
                                new FileOutputStream("new.txt")
                        )) {
                            writer.write(s);
                        }
                    }
                }
                break;
            case "UTF8":
                try (InputStream is = new FileInputStream(fileName)) {
                    byte[] buf = new byte[1000];
                    if(is.read(buf) != -1) {
                        String s = new String(buf, StandardCharsets.UTF_8);
                        try (Writer writer = new OutputStreamWriter(
                                new FileOutputStream("new.txt")
                        )) {
                            writer.write(s);
                        }
                    }
                }
                break;
            default:
                System.out.println("Не удалось распознать введенную вами кодировку.");
        }

    }
}
