package ru.grigorov.Task2;

import java.util.Scanner;

public class GasolineInput {
    public static void main(String[] args) {
        System.out.print("Введите количество литров бензина: ");
        Scanner gasScan = new Scanner(System.in);
        int gasPrice = 43;
        if (gasScan.hasNextInt()){
            int gasVolume = gasScan.nextInt();
            int gasCost = gasPrice * gasVolume;
            if (gasCost >= 0) {
                System.out.println("Стоимость " + gasVolume + " литров бензина составляет " + gasCost + " рублей");
            }
            else {
                System.out.println("Количество литров для заправки не может быть отрицательным. В следующий раз введите положительное число.");
            }
        } else {
            System.out.println("Эта программа считает стоимость только для целого количества литров. В следующий раз введите целое число.");
        }
        gasScan.close();
    }
}
