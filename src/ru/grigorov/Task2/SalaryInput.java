package ru.grigorov.Task2;

import java.util.Scanner;

public class SalaryInput {

    public static void main(String[] args) {
        System.out.print("Введите вашу зарплату: ");
        Scanner salaryInput = new Scanner(System.in);
        double incomeTax = 0.13;
        double totalSalary;
        if (salaryInput.hasNextDouble()){
            totalSalary = salaryInput.nextDouble();
            if (totalSalary >= 0) {
                double cleanSalary = totalSalary - incomeTax * totalSalary;
                System.out.println("При зарплате в " + totalSalary + " рублей \"на руки\" выдается " + cleanSalary + " рублей");
                salaryInput.close();
            }
            else {
                System.out.println("Размер зарплаты не может быть отрицательным. Введите положительное число.");
            }
        } else {
            System.out.println("Что-то не так с введенными вами данными. Введите положительное число, пожалуйста.");
        }
    }
}
