package ru.grigorov.Task2;

import java.util.Scanner;

public class WatchInput2 {
        public static void main(String[] args) {
            System.out.print("Введите количество секунд: ");
            Scanner secInput = new Scanner(System.in);
            double hourAmount;
            if (secInput.hasNextDouble()) {
                double secAmount = secInput.nextDouble();
                hourAmount = secAmount / 3600;
                if (secAmount >= 0) {
                    System.out.println(secAmount + " секунд - это " + hourAmount + " часов");
                    secInput.close();
                }
                else {
                    System.out.println("Вы ввели отрицательное количество секунд. В следующий раз введите положительное.");
                }
            }
            else System.out.println("Вы ввели не число");
        }
    }