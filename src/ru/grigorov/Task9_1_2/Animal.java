package ru.grigorov.Task9_1_2;

public abstract class Animal { //общий класс для всех животных, определающий, что у всех должно быть имя (название вида)

    abstract void getName();
}
