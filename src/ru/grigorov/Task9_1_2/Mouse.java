package ru.grigorov.Task9_1_2;

public class Mouse extends Animal implements Run{
    private String name = "Мышь";
    @Override
    public void getName(){
        System.out.println(name);
    }

    @Override
    public void canRun() {
        System.out.println("Бегаю от норки к норке");
    }
}
