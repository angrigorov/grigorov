package ru.grigorov.Task9_1_2;

public class Duck extends Animal implements Swim, Run, Fly {
    private String name = "Утка";
    @Override
    void getName() {
        System.out.println(name);
    }

    @Override
    public void canFly() {
        System.out.println("Летаю со своей стаей");
    }

    @Override
    public void canRun() {
        System.out.println("Бегаю, переваливаясь с ноги на ногу");
    }

    @Override
    public void canSwim() {
        System.out.println("Плаваю в озере среди кувшинок");
    }
}
