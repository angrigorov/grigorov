package ru.grigorov.Task9_1_2;
/*1. Написать абстрактный класс Animal с абстрактным методом getName. Сделать несколько классов животных, наследников
Animal. Метод getName должен выводит название каждого животного.
2. Написать интерфейсы Fly, Run и Swim чтобы в каждом было по одному методу. Добавить классам животных из предыдущего
задания имплементацию этих интерфейсов. Некоторые животные могут реализовать больше одного интерфейса (утка может и
плавать, и летать и бегать).*/

public class App {
    public static void main(String[] args) {
        Snake terra = new Snake(); //создан объект типа "змея"
        terra.getName();    //запускаем метод, выводящий на экран название вида животного
        terra.canSwim();    //выводим поведение  змеи - она имплементирует интерфейс Swim, значит умеет плавать!
        Mouse mikky = new Mouse();
        mikky.getName();
        mikky.canRun();
        Duck trem = new Duck();
        trem.getName();
        trem.canFly();
        trem.canRun();
        trem.canSwim();
    }
}
