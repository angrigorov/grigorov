package ru.grigorov.Task24;

/*Написать метод, который возвращает множество, в котором удалены все элементы четной длины
из исходного множества.

public Set<String> removeEvenLength(Set<String> set);

Например, для множества ["foo", "buzz", "bar", "fork", "bort", "spoon", "!", "dude"] метод вернет
["foo", "bar", "spoon", "!"]*/

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class AppSet {
    static Set<String> baseSet = new HashSet<>();
    public static void main(String[] args) {
        baseSet.add("Три");
        baseSet.add("Четыре");
        baseSet.add("ТриТриТри");
        baseSet.add("ЧетыреЧетыре");
        System.out.println("Исходный сет: " + baseSet);
        System.out.println("Cет после удаления элементов четной длины: " +
                new AppSet().removeEvenLength(baseSet));
    }

    public Set<String> removeEvenLength(Set<String> set) {
        Set<String> tempSet = new HashSet<>();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            String s = iterator.next().toString();
            System.out.println(s);
            if (s.length()%2 != 0){
                tempSet.add(s);
            } else {
                continue;
             }
        }
        return tempSet;
    }

}
