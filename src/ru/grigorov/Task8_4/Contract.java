package ru.grigorov.Task8_4;

import java.util.Scanner;

public class Contract {
    public static int contractNumber;
    public static String contractDate;
    public static String contractor;
    public static String subjectOfContract;
    public static int sumOfContract;


    public Contract(){
        Scanner scanner1 = new Scanner(System.in);
        System.out.print("Введите номер договора: ");
        contractNumber = scanner1.nextInt();
        System.lineSeparator();
        Scanner scanner2 = new Scanner(System.in);
        System.out.print("Введите дату договора: ");
        contractDate = scanner2.nextLine();
        System.lineSeparator();
        Scanner scanner3 = new Scanner(System.in);
        System.out.print("Введите контрагента: ");
        contractor = scanner3.nextLine();
        System.lineSeparator();
        Scanner scanner4 = new Scanner(System.in);
        System.out.print("Введите предмет договора: ");
        subjectOfContract = scanner4.nextLine();
        System.lineSeparator();
        Scanner scanner5 = new Scanner(System.in);
        System.out.print("Введите сумму договора: ");
        sumOfContract = scanner5.nextInt();
        System.lineSeparator();
    }
}
