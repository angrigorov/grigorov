package ru.grigorov.Task8_4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static ru.grigorov.Task8_4.Contract.*;

public class Converter {
    ActCounter actNumber = new ActCounter(); // Созданием этого объекта я увеличиваю счетчик актов на 1
    public static String actDate;
    public static String actContractor;
    public static String actSubject;
    public static int actSum;

    public void converter(Contract contract){
        actNumber.getActCounter();
        String dateString = LocalDate.now().toString();
        LocalDate date = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        actDate = date.format((DateTimeFormatter.ofPattern("dd MMMM yyyy", new Locale("ru"))));
        actContractor = contractor;
        actSubject = subjectOfContract;
        actSum = sumOfContract;
        System.out.println("По договору № " + contractNumber + " от " + contractDate + " сформирован акт № " +
                actNumber + " от " + actDate + ". Заказчик: " + actContractor + ". Предмет акта: " + actSubject
                + ". Сумма акта: " + actSum + " рублей.");
    }
}
