package ru.grigorov.Task19;

/*Дан текстовый файл определенной структуры, в котором содержится информация о покупках.

        Формат файла:
        Название товара
        количество
        цена

        Необходимо написать программу, которая выведет на экран чек, сформированный из этого файла.
        В чеке должна быть информация:
        название товара:  цена Х кол-во = стоимость

        В конце отчета вывести итоговую стоимость.
        Пример входного файла и вывода прикрепляю к задаче*/

import java.io.*;
import java.util.Locale;
import java.util.Scanner;

public class AppCheck {
    public static void main(String[] args) throws FileNotFoundException {
        String fileName = "products.txt";
        System.out.printf("%-16s %-9s %-10s %-10s %n", "Наименование", "Цена", "Кол-во", "Стоимость");
        System.out.println("===============================================");

        InputStream is = new FileInputStream(fileName);
        PrintStream os = new PrintStream(System.out);

        Scanner scanner = new Scanner(is);
        scanner.useLocale(Locale.ENGLISH);
        float total = 0;

        while (scanner.hasNext()){
            String name = scanner.nextLine();
            float quantity = 0;
            float price = 0;
            if (scanner.hasNextFloat()) {
                quantity = scanner.nextFloat();
                price = scanner.nextFloat();
            } else {
                name = name + scanner.nextLine();
                quantity = scanner.nextFloat();
                price = scanner.nextFloat();
            }
            float total1 = price * quantity;
            total1 = (float) (Math.round(total1 * 100.0) / 100.0);
            os.printf("%-16s %-9s %-10s %-10s %n", name, price, quantity, total1);
            total = total + total1;
        }
        System.out.println("===============================================");
        System.out.printf("%-35s %-10s %n", "Итого:", total);
    }
}
