package ru.grigorov.Task8_3;
/*        3. Реализовать класс в конструкторе которого будет счетчик количества создаваемых объектов. Написать метод для
получения информации о количестве.*/

public class Main {
    public static void main(String[] args) {
        Count k1 = new Count();
        Count k2 = new Count();
        Count k3 = new Count();
        Count k4 = new Count();

        System.out.println("Количество объектов = " + Count.getCounter());
    }
}
