package ru.grigorov.Task17;

import java.io.Serializable;

public class Book implements Serializable {
    private static final long serialVersionUID = 201907050815L;

    private String title;
    private String author;
    private int yearOfPublication;
    private String language;

    public Book(String title, String author, int yearOfPublication, String language) {
        this.title = title;
        this.author = author;
        this.yearOfPublication = yearOfPublication;
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(int yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", yearOfPublication=" + yearOfPublication +
                ", language='" + language + '\'' +
                '}';
    }
}
