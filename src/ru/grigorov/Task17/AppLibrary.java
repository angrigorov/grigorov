package ru.grigorov.Task17;

/*Есть набор объектов типа "Книга". Каждая книга имеет название, автора, год издания. Можно больше, по желанию.

        Программа должна уметь:

        добавлять книгу в библиотеку.
        показывать список книг в библиотеке.
        восстанавливать содержимое библиотеки после перезапуска.
        показывать соответствующее сообщение в случае ошибок.
        Важно!

        потоки обязательно должны закрываться
        отсутствие файла на диске - не ошибка, а частный случай пустой библиотеки*/

import java.io.*;

public class AppLibrary {
    private static final String fileName = "Library.bin";

    public static void main(String[] args) throws Exception{
        Book book1 = new Book("Война и мир", "Толстой Л.Н.", 1850, "ru");
        Book book2 = new Book("Преступление и наказание", "Достоевский Ф.М.", 1860, "ru");
        Book books[] = new Book[]{book1, book2};
        saveBooks(books);
        readBooks(books);
    }

    private static void saveBooks(Book[] books){
        try (ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(fileName)
        )) {
            oos.writeObject(books);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private static void readBooks(Book[] books){
        System.out.println("В библиотеке есть:");
        for (int i = 0; i < books.length; i++) {
            System.out.println(i+1 + ". " + books[i].getAuthor() + " " + books[i].getTitle() + " " + books[i].getYearOfPublication()
            + " " + books[i].getLanguage());

        }
        }
}
