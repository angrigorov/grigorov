package ru.grigorov.Task15;

/*
Написать программу, которая будет создавать, переименовывать, копировать и удалять файл.
Написать рекурсивный обход всех файлов и подкаталогов внутри заданного каталога.
Дополнительное задание (необязательно): программа должна следить за глубиной рекурсии, сдвигая название
файла/каталога на соответствующее количество пробелов.
*/

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileWork {
    public static void main(String[] args) throws IOException {
        new File("donuts/chocolate").mkdirs();
        File chocoDonut = new File("donuts/chocolate/chocoDonut.txt");
        try { //Первый способ пробросить исключение при создании файла - через try/catch
            chocoDonut.createNewFile();
        } catch (IOException e) {
            System.out.println("Файл не создан, так как " + e.getMessage());
        }
        File chocoDonut2 = new File("donuts/chocolate/chocoDonut2.txt");
        Files.copy(chocoDonut.toPath(), chocoDonut2.toPath());
        File chocoDonut3 = new File("donuts/chocolate/chocoDonut3.txt");
        chocoDonut2.renameTo(chocoDonut3);

        new File("donuts/glazed").mkdir();
        new File("donuts/glazed/PinkGlazed.txt").createNewFile();//Второй способ пробросить исключение, вверху
        new File("donuts/glazed/GreenGlazed.txt").createNewFile();

        File donuts = new File("donuts");
        printAllDonuts(donuts);
        eatAllDonuts(donuts);
    }

    private static void printAllDonuts(File donuts) {
        File[] files = donuts.listFiles();
        for (File file : files){
            if (file.isFile()){
                System.out.println("Пончик " + file + " приготовлен");
            } else {
                printAllDonuts(file);
            }
        }
        System.out.println("Коробка с пончиками " + donuts + " готова");
    }

    private static void eatAllDonuts(File donuts) {
        File[] files = donuts.listFiles();
        for (File file : files){
            if (file.isFile()){
                file.delete();
                System.out.println(file + " съеден");
            } else {
                eatAllDonuts(file);
            }
        }
        donuts.delete();
        System.out.println(donuts + " съедена");
    }
}
