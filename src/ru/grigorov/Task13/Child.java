package ru.grigorov.Task13;

import java.util.Scanner;

public class Child {

    static Food dishes[] = Food.values();

    public String giveName() {
        System.out.println("Давайте покормим ребенка. Как его зовут?");
        Scanner scannerName = new Scanner(System.in);
        return scannerName.nextLine();
    }
    public void doEat() throws DontLikeFoodException {
        System.out.println("Итак, " + giveName() + " хочет есть. Сегодня у нас в меню: ");
        for (int i = 1; i <= dishes.length; i++) {
            System.out.println(i + " " + dishes[i-1].getName());
        }
        System.out.println("Выберите номер блюда, которое вы хотите дать ребенку: ");
        Scanner scannerNumberOfDish = new Scanner(System.in);
        int numberOfDish = scannerNumberOfDish.nextInt();
        System.out.println(dishes[numberOfDish - 1].getName() + " на столе.");
        if(dishes[numberOfDish-1].getLike() == 1) {
                System.out.print("Съедено... за обе щеки. Большое");
        } else throw new DontLikeFoodException();
    }
}
