package ru.grigorov.Task13;

public enum Food {
    CARROT("Морковь тертая", 1),
    ZUCCHINI("Кабачок тертый", 0),
    APPLE("Яблоко тертое", 1),
    PORRIDGE("Каша манная", 0),
    CHICKEN("Пюре из курицы", 1);


    private String name;
    private int like;

    Food(String name, int like) {
        this.name = name;
        this.like = like;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }
}
