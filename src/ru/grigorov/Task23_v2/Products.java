package ru.grigorov.Task23_v2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Products implements Basket{
    static List<String> basket = new ArrayList<>();

    public static void main(String[] args) {
        System.out.println("Пустая корзина: ");
        new Products().addProduct("Bread", 10);
        new Products().addProduct("Milk", 8);
        new Products().addProduct("Ketchup", 2);
        System.out.println("Корзина с товарами: " + basket.toString());
        new Products().updateProductQuantity("Bread", 1);
        new Products().updateProductQuantity("Ketchup", 2);
        System.out.println("Корзина после добавления количества товара: " + basket.toString());
        System.out.println("Список продуктов в корзине, без количества: " + new Products().getProducts().toString());
        System.out.println("Сообщаем количество продуктов в корзине: ");
        System.out.println(new Products().getProductQuantity("Bread") + " шт.");
        System.out.println(new Products().getProductQuantity("Milk") + " шт.");
        System.out.println(new Products().getProductQuantity("Ketchup") + " шт.");
        new Products().removeProduct("Milk");
        new Products().removeProduct("Ketchup");
        System.out.println("Корзина после применения removeProduct: " + basket.toString());
        new Products().clear();
        System.out.println("Корзина после очистки: " + basket.toString());
    }

    @Override
    public void addProduct(String product, int quantity) {
        for (int i = 0; i < quantity; i++){
            basket.add(product);
        }
    }

    @Override
    public void removeProduct(String product) {
        System.out.println("удаляем " + product);
        Iterator iterator = basket.iterator();
        while (iterator.hasNext()){
            if (iterator.next().equals(product)){
                iterator.remove();
            }
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        for (int i = 0; i < quantity; i++) {
            basket.add(product);
        }
    }

    @Override
    public void clear() {
        basket.clear();
    }

    @Override
    public List<String> getProducts() {
        List<String> productNames = new ArrayList<>();
        Iterator iterator = basket.iterator();
        while(iterator.hasNext()){
            if (productNames.contains(iterator.next())){
                continue;
            } else {
                String a = iterator.next().toString();
                productNames.add(a);}
        }
        return productNames;
    }

    @Override
    public int getProductQuantity(String product) {
        System.out.print(product + " ");
        return Collections.frequency(basket, product);
    }
}
