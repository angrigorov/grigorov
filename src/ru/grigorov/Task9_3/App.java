package ru.grigorov.Task9_3;
/*3. Написать абстрактный класс Человек реализующий интерфейсы «бежать» и «плавать» (в каждом сделать 1-2 метода).
Сделать несколько наследников этого класса с конкретной реализацией методов, которые объявлены в интерфейсах.*/
public class App {
    public static void main(String[] args) {
        Hawaiian george = new Hawaiian();
        george.typeOfMan();
        george.runAble();
        george.swimInPool();
        george.swimWithSurf();
        System.out.println("");
        Student michael = new Student();
        michael.typeOfMan();
        michael.runAble();
        michael.swimInPool();
        michael.swimWithSurf();
    }
}
