package ru.grigorov.Task9_3;

public interface Swim {
    void swimInPool();
    void swimWithSurf();
}
