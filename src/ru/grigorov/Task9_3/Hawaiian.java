package ru.grigorov.Task9_3;

public class Hawaiian extends Man{
    private String typeOfMan = "Гаваец";

    @Override
    public void typeOfMan() {
        System.out.println(typeOfMan);
    }
    @Override
    public void runAble() {
        System.out.println("Умею бегать (но не люблю)");
    }

    @Override
    public void swimInPool() {
        System.out.println("Умею плавать в бассейне (но не люблю)");
    }

    @Override
    public void swimWithSurf() {
        System.out.println("Умею и люблю плавать на серфе!");
    }
}
