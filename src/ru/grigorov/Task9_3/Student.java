package ru.grigorov.Task9_3;

public class Student extends Man {
    private String typeOfMan = "Студент";

    @Override
    public void typeOfMan() {
        System.out.println(typeOfMan);
    }

    @Override
    public void runAble() {
        System.out.println("Умею и люблю бегать!");
    }

    @Override
    public void swimInPool() {
        System.out.println("Умею и люблю плавать в бассейне!");
    }

    @Override
    public void swimWithSurf() {
        System.out.println("Не умею плавать на серфе, но не отказался бы попробовать!");
    }
}
