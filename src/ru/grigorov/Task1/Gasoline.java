package ru.grigorov.Task1;

public class Gasoline {

    public static void main(String[] args) {
        int gasPrice = 43;
        int gasVolume = 50;
        int gasCost = gasPrice * gasVolume;
        System.out.println("Стоимость " + gasVolume + " литров бензина составляет " + gasCost + " рублей");
    }
}
