package ru.grigorov.Task1;

public class Watch {
    public static void main(String[] args) {
        double secAmount = 3600;
        double hourAmount = secAmount / 3600;
        System.out.println(secAmount + " секунд - это " + hourAmount + " часов");
    }
}
