package ru.grigorov.Task1;

public class Salary {

    public static void main(String[] args) {
        double totalSalary = 70000;
        double incomeTax = 0.13;
        double cleanSalary = totalSalary - incomeTax * totalSalary;
        System.out.println("При зарплате в " + totalSalary + " рублей \"на руки\" выдается " + cleanSalary + " рублей");
    }
}
