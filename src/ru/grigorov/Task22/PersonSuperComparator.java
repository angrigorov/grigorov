package ru.grigorov.Task22;

/*Написать класс PersonSuperComparator,

который имплементит Comparator, но умеет сравнивать по двум параметрам , возраст и имя.

Класс Person теперь содержит два поля int age и String name*/

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class PersonSuperComparator implements Comparator<Person> {

    public int compare(Person a, Person b){
        int compareName = a.getName().compareTo(b.getName());
        if (compareName != 0){
            return compareName;
        }
        return a.getAge().compareTo(b.getAge());
    }

    public static void iterate(List person) {
        Iterator i = person.iterator();
        while (i.hasNext()){
            System.out.println(i.next());
        }
    }
}
