package ru.grigorov.Task22;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List person = new ArrayList();
        person.add(new Person("Alex", 20));
        person.add(new Person("Donald", 20));
        person.add(new Person("Bill", 20));
        person.add(new Person("Avraam", 20));
        person.add(new Person("Alex", 18));
        person.add(new Person("Alex", 20));
        System.out.println("Несортированный список людей: ");
        PersonSuperComparator.iterate(person);
        System.out.println("");
        System.out.println("Cортированный по имени и возрасту список людей: ");
        Collections.sort(person, new PersonSuperComparator());
        PersonSuperComparator.iterate(person);
    }
}
