package ru.grigorov.Task11;
/*Напишите программу которая получает на вход некую строку , после она вызывает метод, заменяющий в строке все вхождения
слова «бяка» на «вырезано цензурой» и выводит результат в консоль!*/
import java.util.Scanner;
import java.util.regex.Pattern;

public class Task11Replace {
    public static void main(String[] args) {
        System.out.println("Введите любую строку. Если там будет бяка, то цензура ее вырежет!");
        Scanner scString = new Scanner(System.in);
        String string = scString.nextLine();
        if (string.toLowerCase().contains("бяка")) {
            String result = Pattern.compile("(?i)бяка", Pattern.UNICODE_CASE).matcher(string).replaceAll("*заменено цензурой*");
            System.out.println("Ваш текст содержал нецензурные выражения, поэтому он был заменен на следующий: ");
            System.out.println(result);
        }
    }
}
