package ru.grigorov.Task25_1;

/*public boolean isUnique(Map<String, String> map);

Написать метод, который возвращает true, если в мапе нет двух и более одинаковых value, и false в противном случае.
Для пустой мапы метод возвращает true.
Например, для метода {Вася=Иванов, Петр=Петров, Виктор=Сидоров, Сергей=Савельев, Вадим=Викторов} метод вернет true,
а для {Вася=Иванов, Петр=Петров, Виктор=Иванов, Сергей=Савельев, Вадим=Петров} метод вернет false.*/

import java.util.HashMap;
import java.util.Map;

public class AppFilterHM {
    private static Map<String,String> map1 = new HashMap<>();
    private static Map<String,String> map2 = new HashMap<>();
    private static Map<String,String> map3 = new HashMap<>();
    public static void main(String[] args) {
        map1.put("Валерий", "Чесноков");
        map1.put("Василий", "Лукин");
        map1.put("Виталий", "Огурцов");
        map2.put("Валерий", "Чесноков");
        map2.put("Василий", "Чесноков");
        map2.put("Виталий", "Огурцов");
        System.out.println("Map1: " + map1);
        System.out.println("Map2: " + map2);
        System.out.println("Map2: " + map3);
        System.out.println("Результат Map1: " + new AppFilterHM().isUnique(map1));
        System.out.println("Результат Map2: " + new AppFilterHM().isUnique(map2));
        System.out.println("Результат Map2: " + new AppFilterHM().isUnique(map3));
    }

    public boolean isUnique(Map<String, String> map){
        Map<String,String> mapTemp = new HashMap<>();
        for (Map.Entry<String,String> entry : map.entrySet()){
            String s = entry.getValue();
            mapTemp.put(s, null);
        }
        if (mapTemp.size() == map.size()) {
            return true;
        } else {return false;
        }
    }
}