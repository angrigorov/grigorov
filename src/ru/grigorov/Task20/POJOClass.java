package ru.grigorov.Task20;

import java.io.Serializable;
import java.util.Date;

public class POJOClass {
    private boolean holiday;
    private Date date;

    public boolean isHoliday() {
        return holiday;
    }

    public void setHoliday(boolean holiday) {
        this.holiday = holiday;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "POJOClass{" +
                "Праздник=" + holiday +
                ", Дата=" + date +
                '}';
    }
}
