package ru.grigorov.Task20;

/*
Есть набор бесплатных сервисов в интернете, предоставляющих данные в формате JSON:
https://github.com/toddmotto/public-apis/blob/master/README.md
Выбрать любой сервис, какой больше нравится, и написать программу, которая с ним взаимодействует.
Класс сериализуемого объекта может содержать не все поля, а только 2-3 ключевых. Например,
для погоды достаточно показать диапазон температур.
Минимальное количество запросов к сервису - 1. Не обязательно реализовывать весь функционал,
предоставляемый сервисом. Достаточного одного любого запроса

находишь в интернете Json
считываешь
Создаешь у себя в проекте класс POJO (что такое - прочитай в интернете)
Данный класс будет состоять из поле, которые называются так же, как и тэги JSon файла
Далее при помощи ObjectMapper (сторонний jar) мапишь json в объект
*/

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.URL;

public class App {
    public static void main(String[] args) throws IOException {
        String tmp = null;
        try {
            URL url = new URL("https://datazen.katren.ru/calendar/day/");
            try (InputStream is = url.openStream();
                 Reader reader = new InputStreamReader(is);
                 BufferedReader br = new BufferedReader(reader)
            ) {
                System.out.println("Вот как выглядит исходный результат: ");
                tmp = br.readLine();
                System.out.println(tmp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        POJOClass obj = objectMapper.readValue(tmp, POJOClass.class);
        System.out.println(obj.getDate() + " " + obj.isHoliday());
    }
}
