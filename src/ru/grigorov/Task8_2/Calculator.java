package ru.grigorov.Task8_2;

import java.util.Scanner;

/*2. Реализовать класс Calculator, который будет содержать статические методы для операций вычитания, сложения, умножения,
        деления и взятия процента от числа. Класс должен работать как с целыми числами, так и с дробями.*/
public class Calculator {
    static double resDouble;
    static int resInt;

    public static void main(String[] args) {
        System.out.println("Введите номер операции, которую вы хотите выполнить");
        System.out.println("1 - вычитание");
        System.out.println("2 - сложение");
        System.out.println("3 - умножение");
        System.out.println("4 - деление");
        System.out.println("5 - взятие процента от числа");
        System.out.println("");
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            int numberOfOperation = scanner.nextInt();
            System.out.println("Введите 2 числа через Enter (можно целые, можно дробные): ");
            Scanner scIntOrDouble1 = new Scanner(System.in);
            Scanner scIntOrDouble2 = new Scanner(System.in);
            if (scIntOrDouble1.hasNextInt() && scIntOrDouble2.hasNextInt()){
                int a = scIntOrDouble1.nextInt();
                int b = scIntOrDouble2.nextInt();
                switch (numberOfOperation){
                    case 1 :
                        System.out.println("Результат операции: " + subtraction(a, b));
                        break;
                    case 2 :
                        System.out.println("Результат операции: " + sum(a, b));
                        break;
                    case 3 :
                        System.out.println("Результат операции: " + multiplication(a, b));
                        break;
                    case 4 :
                        System.out.println("Результат операции: " + division(a, b));
                        break;
                    case 5 :
                        System.out.println("Результат операции: " + percentage(a, b) + "%");
                        break;
                    default :
                        System.out.println("Не удается определить операцию. В следующий раз введите целое число от 1 до 5.");
                        break;
                }
            }
            else {
                double a = scIntOrDouble1.nextDouble();
                double b = scIntOrDouble2.nextDouble();
                switch (numberOfOperation) {
                    case 1:
                        System.out.println("Результат операции: " + subtraction(a, b));
                        break;
                    case 2:
                        System.out.println("Результат операции: " + sum(a, b));
                        break;
                    case 3:
                        System.out.println("Результат операции: " + multiplication(a, b));
                        break;
                    case 4:
                        System.out.println("Результат операции: " + division(a, b));
                        break;
                    case 5:
                        System.out.println("Результат операции: " + percentage(a, b) + "%");
                        break;
                    default:
                        System.out.println("Не удается определить операцию. В следующий раз введите целое число от 1 до 5.");
                        break;
                }
            }
        }
        else System.out.println("Не удается определить операцию. В следующий раз введите целое число от 1 до 5.");
    }

    public static double subtraction (double a, double b){
        resDouble = a - b;
        return resDouble;
    }
    public static int subtraction (int a, int b){
        resInt = a - b;
        return resInt;
    }
    public static double sum (double a, double b){
        resDouble = a + b;
        return resDouble;
    }
    public static int sum (int a, int b){
        resInt = a + b;
        return resInt;
    }
    public static double multiplication (double a, double b){
        resDouble = a * b;
        return resDouble;
    }
    public static int multiplication (int a, int b){
        resInt = a * b;
        return resInt;
    }
    public static double division (double a, double b){
        resDouble = a / b;
        return resDouble;
    }
    public static int division (int a, int b){
        resInt = a / b;
        return resInt;
    }
    public static double percentage (double a, double b){
        resDouble = a / (b / 100);
        return resDouble;
    }
    public static int percentage (int a, int b){
        resInt = a / (b / 100);
        return resInt;
    }
}
