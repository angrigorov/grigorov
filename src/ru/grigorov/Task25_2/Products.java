package ru.grigorov.Task25_2;

import java.util.*;

/*Реализовать класс корзины интернет магазина по следующему интерфейсу, используя реализацию мапы:
interface Basket {
    void addProduct(String product, int quantity);
    void removeProduct(String product);
    void updateProductQuantity(String product, int quantity);
    void clear();
    List<String> getProducts();
    int getProductQuantity(String product);}*/
public class Products implements Basket{
    private static Map<String,Integer> basket = new HashMap<>();

    public static void main(String[] args) {
        System.out.println("Пустая корзина: ");
        new Products().addProduct("Bread", 10);
        new Products().addProduct("Milk", 8);
        new Products().addProduct("Ketchup", 2);
        System.out.println("Корзина с товарами: " + basket);
        new Products().updateProductQuantity("Bread", 1);
        new Products().updateProductQuantity("Ketchup", 2);
        System.out.println("Корзина после добавления количества товара: " + basket);
        System.out.println("Список продуктов в корзине, без количества: " + new Products().getProducts().toString());
        System.out.println("Сообщаем количество продуктов в корзине: ");
        System.out.println(new Products().getProductQuantity("Bread") + " шт.");
        System.out.println(new Products().getProductQuantity("Milk") + " шт.");
        System.out.println(new Products().getProductQuantity("Ketchup") + " шт.");
        new Products().removeProduct("Milk");
        new Products().removeProduct("Ketchup");
        System.out.println("Корзина после применения removeProduct: " + basket);
        new Products().clear();
        System.out.println("Корзина после очистки: " + basket);
    }

    @Override
    public void addProduct(String product, int quantity) {
        basket.put(product,quantity);
    }

    @Override
    public void removeProduct(String product) {
        System.out.println("Удаляем " + product);
        basket.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        int a = basket.get(product);
        int sum = a + quantity;
        basket.put(product, sum);
    }

    @Override
    public void clear() {
        basket.clear();
    }

    @Override
    public List<String> getProducts() {
        Set<String> productsSet = basket.keySet();
        List<String> productList = new ArrayList<>();
        Iterator iterator = productsSet.iterator();
        while(iterator.hasNext()){
            productList.add(iterator.next().toString());
        }
        return productList;
    }

    @Override
    public int getProductQuantity(String product) {
        System.out.print(product + " ");
        return basket.get(product);
    }
}
