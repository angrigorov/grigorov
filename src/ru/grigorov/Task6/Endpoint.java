package ru.grigorov.Task6;
/*ДЗ 6 Смоделировать предметную область
        Используя полученные знания об объектно-ориентированном программировании смоделировать какую-нибудь предметную область.

        Например: банк, университет, библиотека, склад, магазин, пруд и т.д.

        Ограничения:

        Минимум 3 класса
        Наличие нескольких полей и методов
        Использование модификаторов доступа
        Использование принципов ООП*/
/*Этот класс запрашивает ввод имени искомого сотрудника, и запускает метод по выводу его зарплаты из класса Staff
 */
import java.util.Scanner;

import static ru.grigorov.Task6.Company.companyName;

public class Endpoint{
    public static void main(String[] args) {

        Staff robert = new Staff("Роберт", 55000);

        Staff maria = new Staff("Мария", 60000);

        Staff viktor = new Staff("Виктор", 60100);

        System.out.println("В компании \"" + companyName + "\" работают Роберт, Мария и Виктор. Введите имя того сотрудника, чью зарплату вы хотите узнать.");

        Scanner staffScan = new Scanner(System.in);
        String staffName = staffScan.nextLine();

        switch (staffName.toUpperCase()){
            case "РОБЕРТ" :
                robert.saySalary(robert.getName(), robert.getSalary(), companyName);
                break;
            case "МАРИЯ" :
                maria.saySalary(maria.getName(), maria.getSalary(), companyName);
                break;
            case "ВИКТОР" :
                viktor.saySalary(viktor.getName(), viktor.getSalary(), companyName);
                break;
            default :
                System.out.println("Сотрудника с именем \"" + staffName + "\" в компании \"" +
                            companyName + "\" не обнаружено.");
        }
        staffScan.close();
    }
}