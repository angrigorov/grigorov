package ru.grigorov.Task6;
/* В этом классе создаем конструктор для того, чтобы делать сотрудников. Дополнительно создаем метод для вывода на экран зарплаты искомого сотрудника
 */
class Staff extends Company{
    private String name;
    private int salary;
    Staff (String name, int salary){
        this.name = name;
        this.salary = salary;
    }

    public String getName () {
        return name;
    }

    public int getSalary () {
        return salary;
    }

    public void saySalary (String name, int salary, String companyName){
        System.out.println("Сотрудник компании \"" + companyName + "\" " + name + " зарабатывает " + salary + " рублей в месяц.");
    }

}
