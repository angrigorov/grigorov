package ru.grigorov.Task21_2;

//Имеется массив, нужно переставить элементы массива в обратном порядке.

public class AppDZ2 {

    public static void main(String[] args) {
        int[] massive =  {7,13,2,223,232,7,89,4,3};
        System.out.println("Массив до преобразования:");
        printMassive(massive);
        System.out.println("");
        System.out.println("Массив после преобразования:");
        printMassive(reverseMassive(massive));
    }

    private static int[] reverseMassive(int[] massive) {
        for (int i = 0; i < massive.length / 2; i++) {
            int temp = massive[i];
            massive[i] = massive[massive.length - i - 1];
            massive[massive.length - i - 1] = temp;
        }
        return massive;
    }

    private static void printMassive(int[] massive) {
        for (int i = 0; i < massive.length; i++){
            System.out.print(massive[i] + " ");
        }
    }
}
